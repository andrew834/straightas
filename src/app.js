var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var compression = require('compression')


// Try to sanitize the input
var xss = require("xss");
var bodyParser = require('body-parser');
var expressSanitized = require('express-sanitize-escape');

// Ddos protection
var reqLimit = require("express-rate-limit");
var limiter = reqLimit({
	windowMs: 5 * 60 * 1000,
	max: 500,
	message: "You have reached the maximum number of requests. Please try again later."
});

require('./api/models/db');
var indexApi = require('./api/routes/index');


// Routes
var usersRouter = require('./server/routes/user');
var homeRouter = require('./server/routes/home');
var mapRouter = require('./server/routes/map');
var busRouter = require('./server/routes/bus');
var publicEventsRouter = require('./server/routes/public-events');
var app = express();

app.enable("trust proxy");

// Compress all responses
app.use(compression())

app.use(function(req, res, next) {
	res.setHeader('X-Frame-Options', 'DENY');
	res.setHeader('X-XSS-Protection', '1; mode=block');
	//res.setHeader('X-Content-Type-Options', 'nosniff');
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});

// View Engine set to PUG
app.set('views', path.join(__dirname, 'server', 'views'));
app.set('view engine', 'pug');


//app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
	extended: false
}));

app.use(bodyParser.urlencoded({
	extended: true
}));

app.use(bodyParser.json());
app.use(expressSanitized.middleware());
app.use(function(req, res, next) {
	for (var key in req.body) {
		if (Array.isArray(req.body[key]) || typeof req.body[key] === 'object') {
			continue;
		}
		req.body[key] = xss(req.body[key]);
	}
	next();
});

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// Routes to access the application
app.use('/entry', limiter, usersRouter);
app.use('/home', limiter, homeRouter);
app.use('/bus', limiter, busRouter);
app.use('/api', limiter, indexApi);
app.use('/map', limiter, mapRouter);
app.use('/events', limiter, publicEventsRouter);
app.use('/', limiter, function(req, res, next){
	if(req.originalUrl == "/")
		res.redirect("/map");
	else
		next();
});

// Error handling
app.use(function(req, res, next) {
	next(createError(404));
});

app.use(function(err, req, res, next) {
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};
	res.status(err.status || 500);
	res.render('error');
});

module.exports = app;