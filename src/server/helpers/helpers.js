module.exports.error400 = {
	status: "400",
	message: "Bad request"
};

module.exports.error403 = {
	status: "403",
	message: "Forbidden"
};

module.exports.error404 = {
	status: "404",
	message: "Not found"
};

module.exports.error500 = {
	status: "500",
	message: "Internal server error"
};

module.exports.renderError = function(res, err) {
	res.statusCode = err.status;
	res.render('errorPage', {
		error: {
			status: err.status,
			message: err.message
		}
	});
}