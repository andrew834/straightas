var request = require('request');
var VerifyToken = require("../helpers/verifyToken");
var CheckUpravljalec = require('../helpers/verifyToken');

// server se spremeni za REST klice
var paramsApi = {
	server: 'http://localhost:3000',
	apiURI: '/api'
};

if (process.env.NODE_ENV === 'production') {
	paramsApi.server = 'https://tpo-ci-cd-team10.herokuapp.com';
}

module.exports.showLocations = function(req, res) {
	VerifyToken.isUserLoggedIn(req, function(bool){
		CheckUpravljalec.isUserUpravljalec(req, function(isUserUrednik){
			res.render('maps', {
				title: 'Maps',
				isUserLoggedIn: bool,
				isUserUrednik: isUserUrednik
			});	
		});
	});
};