var express = require('express');
var router = express.Router();
var ctrlBus = require('../controllers/bus');

router.get('/', ctrlBus.showBuses);

module.exports = router;