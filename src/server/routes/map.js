var express = require('express');
var router = express.Router();
var ctrlMap = require('../controllers/map');

router.get('/', ctrlMap.showLocations);

module.exports = router;