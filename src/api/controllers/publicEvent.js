var mongoose = require('mongoose');
var PublicEvent = mongoose.model('PublicEvent');

module.exports.addEvent = function(req, res, next) {
	var name = req.body.name;
	var date = req.body.date;
	var organizer = req.body.organizer;
	var desc = req.body.description;

	if (req.role == null || req.userId == null || name == null || date == null || organizer == null || desc == null) {
		res.status(400).send({
			auth: false,
			message: 'No token provided or something is missing in body.'
		});
	} else {
		if (req.role == "urednik") {
			var pEvent = new PublicEvent();
			pEvent.name = name;
			pEvent.date = date;
			pEvent.organizer = organizer;
			pEvent.description = desc;
			pEvent.save(function(error) {

				if (error) {
					console.log(error);
					res.status(500).send({
						message: "Internal server error"
					});
				} else {
					res.status(200).send({
						message: "OK"
					});
				}
			});
		} else {
			res.status(403).send({
				message: "Nimas pravice"
			});
		}
	}
}

module.exports.getEvents = function(req, res, next) {
	PublicEvent.find({}, function(err, events) {
		res.status(200).send(events);
	});
}


module.exports.deleteEvent = function(req, res, next) {
	if (req.role == null || req.userId == null || req.params.eventId == null) {
		res.status(400).send({
			auth: false,
			message: 'No token provided.'
		});
	} else {
		if (req.role == "urednik") {
			PublicEvent.findByIdAndRemove(req.params.eventId).exec(function(error, event) {
				if (error) {
					console.log(error);
					res.status(500).send({
						message: 'Internal sever error'
					});
				} else {
					res.status(200).send({
						message: 'OK'
					});
				}
			});
		} else {
			res.status(403).send({
				auth: false,
				message: 'Nimas pravice.'
			});
		}
	}
}