var mongoose = require('mongoose');
var AdminNotification = mongoose.model('AdminNotification');

module.exports.uploadNotification = function(req, res, next) {
	if (req.role == null || req.userId == null || req.body.message == null) {
		res.status(400).send({
			message: "Bad request, missing token role or id"
		});
	} else {
		if (req.role == "admin") {
			var notification = AdminNotification();
			notification.message = req.body.message;
			notification.save(function(error) {
				if (error) {
					console.log(error);
					res.status(500).send({
						message: "internal server error"
					});
				} else {
					res.status(200).send({
						message: "OK",
					});
				}
			});
		} else {
			res.status(403).send({
				message: "Nimas pravice"
			});
		}
	}
}

module.exports.getLastNotification = function(req, res, next) {
	if (req.role == null || req.userId == null) {
		res.status(400).send({
			message: "Bad request, missing token role or id"
		});
	} else {
		AdminNotification.find().sort({
			_id: -1
		}).limit(1).then(function(notification) {
			res.status(200).send({
				message: "OK",
				data: notification
			});
		}, function(error) {
			console.log(error);
			res.status(500).send({
				message: "internal server error"
			});
		});


	}
};