var mongoose = require('mongoose');
var User = mongoose.model('User');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
	odgovor.status(status);
	odgovor.json(vsebina);
};

module.exports.registerUser = function(req, res) {
	if (req.body.password == null || req.body.email == null) {
		res.status(400).send({
			message: "Bad request, Check your input fields (body)"
		});
	} else {
		var user = new User();
		user.setPassword(req.body.password);
		user.email = req.body.email;
		user.role = 'Registriran uporabnik';
		user.calendar = [];
		user.timetable = [];
		user.todo = [];

		user.save(function(error) {
			if (error) {
				console.log(error);
				res.status(500).send({
					message: "internal server error"
				});
			} else {
				res.status(200).send({
					message: "OK",
					token: user.generateToken()
				});
			}
		});
	}
}

module.exports.getCurrentUser = function(req, res, next) {
	var userID = req.userId;
	if (userID == null) {
		res.status(400).send({
			message: "Bad request, missin id in token"
		});
	} else {
		User.findById(userID).exec(function(error, user) {
			if (error) {
				console.log(error);
				res.status(500).send({
					message: "internal server error"
				});
			} else {
				res.status(200).send({
					calendar: user.calendar,
					timetable: user.timetable,
					todo: user.todo
				});
			}
		});
	}
}


module.exports.loginUser = function(req, res) {
	if (req.body.password == null || req.body.email == null) {
		res.status(400).send({
			message: "Bad request, Check your input fields (body)"
		});
	} else {
		User.findOne({
			email: req.body.email
		}, function(error, user) {

			if (error) {
				res.status(500).send({
					message: "Internal server error"
				});
			} else {
				if (user) {
					if (user.checkPassword(req.body.password)) {
						res.status(200).send({
							message: "OK",
							token: user.generateToken()
						});
					} else {
						res.status(400).send({
							message: "Wrong username or password"
						});
					}
				} else {
					res.status(404).send({
						message: "User not found"
					});
				}
			}
		});
	}
}

module.exports.changePassword = function(req, res, next) {
	if (req.userId == null || req.body.oldpassword == null || req.body.newpassword == null) {
		res.status(400).send({
			message: "Bad request"
		});
		return;
	}
	User
		.findById(req.userId)
		.exec(
			function(napaka, user) {
				if (!user) {
					res.status(404).send({
						message: "User id must be given."
					});
					return;
				} else if (napaka) {
					res.status(500).send({
						message: "Internal server error."
					});
					return;
				}

				if (user.checkPassword(req.body.oldpassword)) {
					user.setPassword(req.body.newpassword);
				} else {
					res.status(400).send({
						message: "Invalid password."
					});
					return;
				}

				user.save(function(napaka, user) {
					if (napaka) {
						res.status(500).send({
							message: "Internal server error."
						});
						return;
					} else {
						res.status(200).send({
							message: "OK",
							data: user
						});
					}
				});
			}
		);
}


module.exports.addEvent = function(zahteva, odgovor, next) {
	var userID = zahteva.userId;
	if (!zahteva.params.idUser || !userID) {
		vrniJsonOdgovor(odgovor, 400, {
			status: 400,
			message: "User id must be given."
		});
		return;
	}
	User
		.findById(userID)
		.exec(
			function(napaka, user) {
				if (!user) {
					vrniJsonOdgovor(odgovor, 404, {
						status: 404,
						message: "No user with given id."
					});
					return;
				}

				user.calendar.push({
					title: zahteva.body.title,
					startTime: zahteva.body.startTime,
					endTime: zahteva.body.endTime,
					description: zahteva.body.description
				})
				user.save(function(napaka, user) {
					if (napaka) {
						vrniJsonOdgovor(odgovor, 400, napaka);
					} else {
						vrniJsonOdgovor(odgovor, 200, user.calendar);
					}
				});
			}
		);
};

module.exports.updateEvent = function(zahteva, odgovor, next) {
	var userID = zahteva.userId;
	if (!zahteva.params.idUser || !userID) {
		vrniJsonOdgovor(odgovor, 400, {
			status: 400,
			message: "User id must be given."
		});
		return;
	}
	User
		.findById(userID)
		.select('calendar')
		.exec(
			function(napaka, user) {
				if (!user) {
					vrniJsonOdgovor(odgovor, 404, {
						"sporočilo": "Ne najdem uporabnika."
					});
					return;
				} else if (napaka) {
					vrniJsonOdgovor(odgovor, 500, napaka);
					return;
				}
				if (user.calendar && user.calendar.length > 0) {
					var calendar =
						user.calendar.id(zahteva.params.idEvent);
					if (!calendar) {
						vrniJsonOdgovor(odgovor, 404, {
							"sporočilo": "Eventa ne najdem."
						});
					} else {
						calendar.title = zahteva.body.title;
						calendar.startTime = zahteva.body.startTime;
						calendar.endTime = zahteva.body.endTime;
						calendar.description = zahteva.body.description;
						calendar.save(function(napaka, userK) {
							
							if (napaka) {
								vrniJsonOdgovor(odgovor, 400, napaka);
							} else {
								vrniJsonOdgovor(odgovor, 200, user.calendar);
							}

						});
						user.save();
					}
				} else {
					vrniJsonOdgovor(odgovor, 404, {
						"sporočilo": "Ni uporabnikov za urejanje."
					});
				}
			}
		);
};

module.exports.removeEvent = function(zahteva, odgovor, next) {
	var userID = zahteva.userId;
	if (!zahteva.params.idUser || !userID) {
		vrniJsonOdgovor(odgovor, 400, {
			status: 400,
			message: "User id must be given."
		});
		return;
	}
	User
		.findByIdAndUpdate(userID, {
				$pull: {
					calendar: {
						_id: zahteva.params.idEvent
					}
				}
			}, {
				multi: false
			},
			function(napaka, user) {
				if (napaka) {
					vrniJsonOdgovor(odgovor, 400, napaka);
				} else {
					var odg = {
						message: "OK"
					};
					vrniJsonOdgovor(odgovor, 200, odg);
				}
			}

		)
};

module.exports.eventAll = function(zahteva, odgovor, next) {
	var userID = zahteva.userId;
	if (zahteva.params && zahteva.params.idUser && userID) {
		User
			.findById(userID)
			.exec(function(napaka, user) {
				if (!user) {
					vrniJsonOdgovor(odgovor, 404, {
						status: 404,
						message: "No user with given id."
					});
					return;
				} else if (napaka) {
					vrniJsonOdgovor(odgovor, 500, napaka);
				} else {
					vrniJsonOdgovor(odgovor, 200, user.calendar);
				}

			});
	} else {
		vrniJsonOdgovor(odgovor, 400, {
			status: 400,
			message: "Missing id."
		});
	};
};

module.exports.getEventById = function(req, res, next) {
	var userID = req.userId;
	if (userID == null || req.params.idEvent == null) {
		vrniJsonOdgovor(odgovor, 400, {
			status: 400,
			message: "User id must be given."
		});
		return;
	}

	User.findById(userID).exec(function(error, user) {
		var myEvent = user.calendar.id(req.params.idEvent)
		if (myEvent == null) {
			res.status(404).send({
				message: "no calendar found"
			});
		} else {
			res.status(200).send({
				message: "OK",
				data: myEvent
			});
		}
	});
}

module.exports.addSlot = function(zahteva, odgovor, next) {
	var userID = zahteva.userId;
	if (!zahteva.params.idUser && !userID) {
		vrniJsonOdgovor(odgovor, 400, {
			status: 400,
			message: "User id must be given."
		});
		return;
	}
	User
		.findById(userID)
		.exec(
			function(napaka, user) {
				if (!user) {
					vrniJsonOdgovor(odgovor, 404, {
						status: 404,
						message: "No user with given id."
					});
					return;
				}

				user.timetable.push({
					title: zahteva.body.title,
					date: zahteva.body.date
				})
				user.save(function(napaka, user) {
					if (napaka) {
						vrniJsonOdgovor(odgovor, 400, napaka);
					} else {
						vrniJsonOdgovor(odgovor, 200, user.timetable[user.timetable.length - 1]);
					}
				});
			}
		);
};

module.exports.updateSlot = function(zahteva, odgovor, next) {
	var userID = zahteva.userId;
	if (!zahteva.params.idUser || !userID) {
		vrniJsonOdgovor(odgovor, 400, {
			status: 400,
			message: "User id must be given."
		});
		return;
	}
	User
		.findById(userID)
		.select('timetable')
		.exec(
			function(napaka, user) {
				if (!user) {
					vrniJsonOdgovor(odgovor, 404, {
						"sporočilo": "Ne najdem uporabnika."
					});
					return;
				} else if (napaka) {
					vrniJsonOdgovor(odgovor, 500, napaka);
					return;
				}
				if (user.timetable && user.timetable.length > 0) {
					var timetable =
						user.timetable.id(zahteva.params.idSlot);
					if (!timetable) {
						vrniJsonOdgovor(odgovor, 404, {
							"sporočilo": "Eventa ne najdem."
						});
					} else {
						timetable.title = zahteva.body.title;
						timetable.datee = zahteva.body.date;
						timetable.save(function(napaka, user) {
							if (napaka) {
								vrniJsonOdgovor(odgovor, 400, napaka);
							} else {
								vrniJsonOdgovor(odgovor, 200, timetable);
							}

						});
						user.save();
					}
				} else {
					vrniJsonOdgovor(odgovor, 404, {
						"sporočilo": "Ni uporabnikov za urejanje."
					});
				}
			}
		);
};

module.exports.removeSlot = function(zahteva, odgovor, next) {
	var userID = zahteva.userId;
	if (!zahteva.params.idUser && !userID) {
		vrniJsonOdgovor(odgovor, 400, {
			status: 400,
			message: "User id must be given."
		});
		return;
	}
	User
		.findByIdAndUpdate(userID, {
				$pull: {
					timetable: {
						_id: zahteva.params.idSlot
					}
				}
			}, {
				multi: false
			},
			function(napaka, user) {
				if (napaka) {
					vrniJsonOdgovor(odgovor, 400, napaka);
				} else {
					vrniJsonOdgovor(odgovor, 200, user);
				}
			}

		)
};

module.exports.slotAll = function(zahteva, odgovor, next) {
	var userID = zahteva.userId;
	if (zahteva.params && zahteva.params.idUser && userID) {
		User
			.findById(userID)
			.exec(function(napaka, user) {
				if (!user) {
					vrniJsonOdgovor(odgovor, 404, {
						status: 404,
						message: "No user with given id."
					});
					return;
				} else if (napaka) {
					vrniJsonOdgovor(odgovor, 500, napaka);
				} else {
					vrniJsonOdgovor(odgovor, 200, user.timetable);
				}

			});
	} else {
		vrniJsonOdgovor(odgovor, 400, {
			status: 400,
			message: "Missing id."
		});
	};
};

module.exports.addNote = function(zahteva, odgovor, next) {
	var userID = zahteva.userId;
	if (!zahteva.params.idUser && !userID) {
		vrniJsonOdgovor(odgovor, 400, {
			status: 400,
			message: "User id must be given."
		});
		return;
	}
	User
		.findById(userID)
		.exec(
			function(napaka, user) {
				if (!user) {
					vrniJsonOdgovor(odgovor, 404, {
						status: 404,
						message: "No user with given id."
					});
					return;
				}

				user.todo.push({
					text: zahteva.body.text
				})
				user.save(function(napaka, user) {
					if (napaka) {
						vrniJsonOdgovor(odgovor, 400, napaka);
					} else {
						vrniJsonOdgovor(odgovor, 200, user.todo[user.todo.length - 1]);
					}
				});
			}
		);
};

module.exports.updateNote = function(zahteva, odgovor, next) {
	var userID = zahteva.userId;
	if (!zahteva.params.idUser && !userID) {
		vrniJsonOdgovor(odgovor, 400, {
			status: 400,
			message: "User id must be given."
		});
		return;
	}
	User
		.findById(userID)
		.select('todo')
		.exec(
			function(napaka, user) {
				if (!user) {
					vrniJsonOdgovor(odgovor, 404, {
						"sporočilo": "Ne najdem uporabnika."
					});
					return;
				} else if (napaka) {
					vrniJsonOdgovor(odgovor, 500, napaka);
					return;
				}
				if (user.todo && user.todo.length > 0) {
					var todo =
						user.todo.id(zahteva.params.idNote);
					if (!todo) {
						vrniJsonOdgovor(odgovor, 404, {
							"sporočilo": "Nota ne najdem."
						});
					} else {
						todo.text = zahteva.body.text;
						todo.save(function(napaka, user) {
							if (napaka) {
								vrniJsonOdgovor(odgovor, 400, napaka);
							} else {
								vrniJsonOdgovor(odgovor, 200, todo);
							}

						});
						user.save();
					}
				} else {
					vrniJsonOdgovor(odgovor, 404, {
						"sporočilo": "Ni notov za urejanje."
					});
				}
			}
		);
};

module.exports.removeNote = function(zahteva, odgovor, next) {
	var userID = zahteva.userId;
	if (!zahteva.params.idUser && !userID) {
		vrniJsonOdgovor(odgovor, 400, {
			status: 400,
			message: "User id must be given."
		});
		return;
	}
	User
		.findByIdAndUpdate(userID, {
				$pull: {
					todo: {
						_id: zahteva.params.idNote
					}
				}
			}, {
				multi: false
			},
			function(napaka, user) {
				if (napaka) {
					vrniJsonOdgovor(odgovor, 400, napaka);
				} else {
					vrniJsonOdgovor(odgovor, 200, {
						message: "OK"
					});
				}
			}

		)
};

module.exports.noteAll = function(zahteva, odgovor, next) {
	var userID = zahteva.userId;
	if (zahteva.params && zahteva.params.idUser && userID) {
		User
			.findById(userID)
			.exec(function(napaka, user) {
				if (!user) {
					vrniJsonOdgovor(odgovor, 404, {
						status: 404,
						message: "No user with given id."
					});
					return;
				} else if (napaka) {
					vrniJsonOdgovor(odgovor, 500, napaka);
				} else {
					vrniJsonOdgovor(odgovor, 200, user.todo);
				}

			});
	} else {
		vrniJsonOdgovor(odgovor, 400, {
			status: 400,
			message: "Missing id."
		});
	};
};