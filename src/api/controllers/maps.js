var MongoClient = require('mongodb').MongoClient;

module.exports.getAll = function(req, res) {
	MongoClient.connect('mongodb://admin:admin123@ds349045.mlab.com:49045/baza1', function(err, client) {
		var db = client.db('baza1');
		db.collection('restaurants').find({}).toArray(function(err, result) {
			if (err) {
				res.status(500).send({
					message: "Internal server error",
					data: []
				});
			}else{
				res.status(200).send({
					message: "OK",
					data: result
				});
			}
			client.close();
		});
	});
}