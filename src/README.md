# Dokumentacija

Na kratko napisana dokumentacija za zaledni sistem, avtentikacijo in REST api.

## Zaledni sistem

Poti za dostop do vsebine:

- **/** : preusmeri na '/map'
- **/entry** prikaže login in register zaslon
- **/entry/login** za poskus logina
- **/entry/register** za poskus registracije
- **/entry/settings** če si prijavljen, si lahko spremeniš geslo
- **/map** prikaz restavracij
- **/bus** prikaz avtobusov
- **/logout** odjavi uporabnika
- **/events/public** prikaze vse public evente uporabniku
- **/events/preview** prikaze uredniku evente in mu omogoci urejanje
- **/home** glavna stran, ki vsebuje zahtevane funkcionalnosti koledarja, eventov in todojev

Ob loginu oziroma registraciji se uporabniku dodeli token, ki se uporablja za navigacijo skozi aplikacijo. Funkcionalosti na **/home** so omejene in jih lahko uporabljajo le prijavljeni uporabniki.

## Avtentikacija in avtorizacija

Avtentikacija in avtorizacija je implementirana s pomočjo JWT žetonov. Ob loginu oziroma registraciji se uporabniku kreira žeton, ki mu omogoča dostop do vvsebine namenjene le prijavljenim uporabnikom. Strežnik uporabniku shrani žeton v njegov piškotek. V primeru, da se zahteva vsebina, ki je namenjena prijavljenim uporabnikom, se pošlje žeton v zaglavju HTTP zahtevka kot *x-access-token*. V žetonu se nahaja ID uporabnika, datum poteka žetona in pravica. Pravica v žetonu se uporablja za omejevanje dostopa do določene vsebine (urednik lahko kreira novee javne dogodke, medtem ko registrirani uporabnik ne).

## Rest API

Več o REST-u in njegovih klicih najdete na spodnji povezavi.
https://documenter.getpostman.com/view/7614761/S1TPaL6Z?version=latest

# Zagon aplikacije

Premaknite se v **/src** direktorij in naredite npm install ter npm start.